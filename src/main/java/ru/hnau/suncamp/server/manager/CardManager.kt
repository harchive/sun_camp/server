package ru.hnau.suncamp.server.manager

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.Example
import org.springframework.stereotype.Component
import ru.hnau.jutils.possible.Possible
import ru.hnau.suncamp.common.api.error.ApiErrorException
import ru.hnau.suncamp.common.api.error.ApiErrorType
import ru.hnau.suncamp.common.data.Card
import ru.hnau.suncamp.common.data.User
import ru.hnau.suncamp.server.data.db.CardDB
import ru.hnau.suncamp.server.data.db.UserDB
import ru.hnau.suncamp.server.data.repository.CardRepository
import ru.hnau.suncamp.server.data.repository.UserRepository
import ru.hnau.suncamp.server.utils.isUuid
import ru.hnau.suncamp.server.utils.log.JSessionLog
import java.util.NoSuchElementException


@Component
class CardManager {

    @Autowired
    lateinit var cardRepository: CardRepository

    @Autowired
    lateinit var userManager: UserManager

    fun register(authToken: String, cardUuid: String, sessionLog: JSessionLog) {

        val logger = sessionLog.getMethodLog(CardManager::class.java, "register")

        val user = userManager.findUserByAuthToken(authToken, sessionLog)

        if (!isUuid(cardUuid)) {
            logger.w("Incorrect card UUID: '$cardUuid'")
            throw ApiErrorException(ApiErrorType.INCORRECT_CARD_UUID)
        }

        try {
            cardRepository.findOne(Example.of(CardDB(uuid = cardUuid))).get()
            logger.w("Card with uuid '$cardUuid' already registered")
            throw ApiErrorException(ApiErrorType.LOGIN_ALREADY_REGISTERED)
        } catch (ex: NoSuchElementException) {
        }

        val card = CardDB(
                uuid = cardUuid,
                associatedUserLogin = user.login
        )

        cardRepository.save(card)

        logger.d("Card $card registered")

    }

    fun remove(authToken: String, cardUuid: String, sessionLog: JSessionLog) {

        val logger = sessionLog.getMethodLog(CardManager::class.java, "remove")

        val user = userManager.findUserByAuthToken(authToken, sessionLog)

        if (!isUuid(cardUuid)) {
            logger.w("Incorrect card UUID: '$cardUuid'")
            throw ApiErrorException(ApiErrorType.INCORRECT_CARD_UUID)
        }

        val possibleCard = Possible.tryCatch { cardRepository.findOne(Example.of(CardDB(uuid = cardUuid))).get() }
        val card = possibleCard.handle(
                onSuccess = { it },
                onError = {
                    throw ApiErrorException(ApiErrorType.CARD_NOT_FOUND)
                }
        )

        if (card.associatedUserLogin != user.login) {
            throw ApiErrorException(ApiErrorType.PERMISSION_DENIED)
        }

        cardRepository.delete(card)
    }

    fun getList(authToken: String, sessionLog: JSessionLog): List<Card> {

        val user = userManager.findUserByAuthToken(authToken, sessionLog)

        val userCardsDB = cardRepository.findAll(Example.of(CardDB(associatedUserLogin = user.login)))
        val userCards = userCardsDB.map { it.card }

        val logger = sessionLog.getMethodLog(CardManager::class.java, "getList")
        logger.d("User '${user.login}' cards: $userCards")

        return userCards
    }

    fun getUserByCardUuid(cardUuid: String, sessionLog: JSessionLog): User {

        val logger = sessionLog.getMethodLog(CardManager::class.java, "getUserByCardUuid")

        if (!isUuid(cardUuid)) {
            logger.w("Incorrect card UUID: '$cardUuid'")
            throw ApiErrorException(ApiErrorType.INCORRECT_CARD_UUID)
        }

        val card = try {
            cardRepository.findOne(Example.of(CardDB(uuid = cardUuid))).get()
        } catch (ex: NoSuchElementException) {
            logger.w("No card found for cardUuid: '$cardUuid'")
            throw ApiErrorException(ApiErrorType.CARD_NOT_FOUND)
        }

        logger.d("Card $card found for cardUuid: '$cardUuid'")

        val userLogin = card.associatedUserLogin!!

        val user = userManager.findUserByLogin(userLogin, sessionLog)

        return user.user
    }


}