package ru.hnau.suncamp.server.manager

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.Example
import org.springframework.stereotype.Component
import ru.hnau.suncamp.common.api.error.ApiErrorException
import ru.hnau.suncamp.common.api.error.ApiErrorType
import ru.hnau.suncamp.common.api.response.SubordinateStorageInfo
import ru.hnau.suncamp.common.data.StorageManager
import ru.hnau.suncamp.common.data.User
import ru.hnau.suncamp.common.data.UserType
import ru.hnau.suncamp.server.data.db.StorageManagerDB
import ru.hnau.suncamp.server.data.repository.StorageManagerRepository
import ru.hnau.suncamp.server.manager.transaction.BalanceManager
import ru.hnau.suncamp.server.manager.transaction.TransactionManager
import ru.hnau.suncamp.server.utils.log.JSessionLog


@Component
class StorageManagerManager {

    @Autowired
    lateinit var storageManagerRepository: StorageManagerRepository

    @Autowired
    lateinit var userManager: UserManager

    @Autowired
    lateinit var transactionManager: TransactionManager

    @Autowired
    lateinit var balanceManager: BalanceManager

    fun checkIsAllowUserToManageStorage(masterLogin: String, slaveLogin: String) =
            storageManagerRepository.exists(
                    Example.of(
                            StorageManagerDB(
                                    StorageManager(
                                            masterUserLogin = masterLogin,
                                            slaveUserLogin = slaveLogin
                                    )
                            )
                    )
            )

    fun addStorageManager(
            authToken: String,
            managerLogin: String,
            sessionLog: JSessionLog
    ) {
        val storage = userManager.findUserByAuthToken(authToken, sessionLog)
        if (storage.type != UserType.storage) {
            throw ApiErrorException(ApiErrorType.UNABLE_TO_MANAGE_NOT_STORAGE_USER)
        }

        val manager = userManager.findUserByLogin(managerLogin, sessionLog)
        if (manager.type == null || !manager.type.allowManageStoragesAccounts) {
            throw ApiErrorException(ApiErrorType.UNSUPPORTED_USER_TYPE_FOR_MANAGING_STORAGE)
        }

        if (checkIsAllowUserToManageStorage(
                        masterLogin = managerLogin,
                        slaveLogin = storage.login!!
                )
        ) {
            return
        }

        val storageManager = StorageManagerDB(
                masterUserLogin = managerLogin,
                slaveUserLogin = storage.login
        )
        storageManagerRepository.save(storageManager)
    }

    fun removeStorageManager(
            authToken: String,
            managerLogin: String,
            sessionLog: JSessionLog
    ) {
        val storage = userManager.findUserByAuthToken(authToken, sessionLog)
        val manager = userManager.findUserByLogin(managerLogin, sessionLog)

        storageManagerRepository.delete(
                StorageManagerDB(
                        StorageManager(
                                masterUserLogin = manager.login!!,
                                slaveUserLogin = storage.login!!
                        )
                )
        )

    }

    fun getSubordinateList(
            authToken: String,
            sessionLog: JSessionLog
    ): List<SubordinateStorageInfo> {

        val manager = userManager.findUserByAuthToken(authToken, sessionLog)
        if (manager.type == null || !manager.type.allowManageStoragesAccounts) {
            throw ApiErrorException(ApiErrorType.UNSUPPORTED_USER_TYPE_FOR_MANAGING_STORAGE)
        }

        return storageManagerRepository
                .findAll()
                .mapNotNull {
                    val storageManager = it.storageManager
                    val masterLogin = storageManager.masterUserLogin
                    if (masterLogin != manager.login) {
                        return@mapNotNull null
                    }
                    val storageLogin = storageManager.slaveUserLogin
                    val storageBalance = balanceManager.getBalanceByLoginInner(storageLogin, sessionLog)
                    val storageTransactions = transactionManager.getTransactionsInner(storageLogin).let { it.negativeTransactions + it.positiveTransactions }
                    return@mapNotNull SubordinateStorageInfo(
                            storageLogin,
                            storageBalance,
                            storageTransactions
                    )

                }

    }

}