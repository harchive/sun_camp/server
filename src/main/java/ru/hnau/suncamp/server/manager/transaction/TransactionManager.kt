package ru.hnau.suncamp.server.manager.transaction

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.Example
import org.springframework.stereotype.Component
import ru.hnau.suncamp.common.api.error.ApiErrorException
import ru.hnau.suncamp.common.api.error.ApiErrorType
import ru.hnau.suncamp.common.data.Transaction
import ru.hnau.suncamp.common.data.TransactionCategory
import ru.hnau.suncamp.server.data.db.TransactionDB
import ru.hnau.suncamp.server.data.repository.TransactionRepository
import ru.hnau.suncamp.server.manager.PermissionsManager
import ru.hnau.suncamp.server.manager.UserManager
import ru.hnau.suncamp.server.utils.log.JSessionLog
import java.util.*


@Component
class TransactionManager {

    @Autowired
    lateinit var transactionRepository: TransactionRepository

    @Autowired
    lateinit var userManager: UserManager

    @Autowired
    lateinit var balanceManager: BalanceManager

    @Autowired
    lateinit var permissionsManager: PermissionsManager

    @Autowired
    lateinit var transactionLockManager: TransactionLockManager

    fun execute(
            executeTransactionParam: ExecuteTransactionParam,
            sessionLog: JSessionLog
    ) = doInLock {
        throwTransactionsIsDisabledException()
        validateTransaction(executeTransactionParam, true, sessionLog)
        executeInner(executeTransactionParam, sessionLog)
    }.transaction

    fun checkRollback(
            authToken: String,
            transactionId: Long,
            sessionLog: JSessionLog
    ) {
        val transaction = getTransactionByIdInner(transactionId, sessionLog)
        validateTransaction(
                executeTransactionParam = ExecuteTransactionParam(
                        authToken = authToken,
                        comment = transaction.comment!!,
                        associatedTransactionId = transactionId,
                        category = TransactionCategory.rollback,
                        amount = transaction.amount!!,
                        toUserLogin = transaction.fromUserLogin!!,
                        fromUserLogin = transaction.toUserLogin!!
                ),
                validateBalance = true,
                sessionLog = sessionLog
        )
    }

    fun rollback(
            authToken: String,
            transactionId: Long,
            sessionLog: JSessionLog
    ): Transaction {
        val transaction = getTransactionByIdInner(transactionId, sessionLog)
        return execute(
                executeTransactionParam = ExecuteTransactionParam(
                        authToken = authToken,
                        comment = transaction.comment!!,
                        associatedTransactionId = transactionId,
                        category = TransactionCategory.rollback,
                        amount = transaction.amount!!,
                        toUserLogin = transaction.fromUserLogin!!,
                        fromUserLogin = transaction.toUserLogin!!
                ),
                sessionLog = sessionLog
        )
    }

    fun getList(authToken: String, login: String, sessionLog: JSessionLog): List<Transaction> {

        val master = userManager.findUserByAuthToken(authToken, sessionLog)
        val slave = userManager.findUserByLogin(login, sessionLog)

        val hasPermissions = permissionsManager.allowUserManageUser(master, slave)
        if (!hasPermissions) {
            throw ApiErrorException(ApiErrorType.PERMISSION_DENIED)
        }

        return getTransactionsInner(login).let { it.positiveTransactions + it.negativeTransactions }

    }

    fun fromStorageToUsers(
            executeTransactionParam: ExecuteTransactionParam,
            sessionLog: JSessionLog
    ) = doInLock {

        throwTransactionsIsDisabledException()

        val usersLogins = executeTransactionParam.toUsersLogins

        val transactionsParams = usersLogins.map { toUserLogin ->
            executeTransactionParam.copy(toUserLogin = toUserLogin)
        }

        transactionsParams.forEach {
            validateTransaction(it, false, sessionLog)
        }

        val totalAmount = executeTransactionParam.amount * usersLogins.size
        val storageBalance = balanceManager.getBalanceByLoginInner(executeTransactionParam.fromUserLogin, sessionLog)
        if (storageBalance < totalAmount) {
            throw ApiErrorException(ApiErrorType.ACCOUNT_OUTCOME_INSUFFICIENT_BALANCE)
        }

        transactionsParams.forEach {
            executeInner(it, sessionLog)
        }

    }

    fun fromUsersToStorage(
            executeTransactionParam: ExecuteTransactionParam,
            sessionLog: JSessionLog
    ) = doInLock {

        val transactionsParams = executeTransactionParam.fromUsersLogins.map { fromUserLogin ->
            executeTransactionParam.copy(fromUserLogin = fromUserLogin)
        }

        transactionsParams.forEach {
            validateTransaction(it, true, sessionLog)
        }

        transactionsParams.forEach {
            executeInner(it, sessionLog)
        }

    }

    private fun <R> doInLock(action: () -> R): R {
        transactionLockManager.lock()
        return try {
            action.invoke()
        } finally {
            transactionLockManager.unlock()
        }
    }

    private fun executeInner(
            executeTransactionParam: ExecuteTransactionParam,
            sessionLog: JSessionLog
    ): TransactionDB {
        val initiator = userManager.findUserByAuthToken(executeTransactionParam.authToken, sessionLog)
        val transaction = TransactionDB(
                fromUserLogin = executeTransactionParam.fromUserLogin,
                toUserLogin = executeTransactionParam.toUserLogin,
                initiatorUserLogin = initiator.login,
                amount = executeTransactionParam.amount,
                category = executeTransactionParam.category,
                associatedTransactionId = executeTransactionParam.associatedTransactionId?.toBigInteger(),
                comment = executeTransactionParam.comment,
                timestamp = System.currentTimeMillis()
        )

        transactionRepository.save(transaction)

        UserBalanceCache.inc(executeTransactionParam.fromUserLogin, -executeTransactionParam.amount)
        UserBalanceCache.inc(executeTransactionParam.toUserLogin, executeTransactionParam.amount)

        val logger = sessionLog.getMethodLog(TransactionManager::class.java, "execute")
        logger.d("New transaction: $transaction")

        return transaction
    }

    fun getTransactionsInner(login: String): TransactionsList {

        val userPositiveTransactionsDB = transactionRepository.findAll(Example.of(TransactionDB(toUserLogin = login)))
        val userNegativeTransactionsDB = transactionRepository.findAll(Example.of(TransactionDB(fromUserLogin = login)))

        return TransactionsList(
                positiveTransactions = userPositiveTransactionsDB.map { it.transaction },
                negativeTransactions = userNegativeTransactionsDB.map { it.transaction }
        )

    }

    fun getTransactionByIdInner(transactionId: Long, sessionLog: JSessionLog): TransactionDB {

        val logger = sessionLog.getMethodLog(TransactionManager::class.java, "getTransactionByIdInner")

        return try {
            transactionRepository.findOne(Example.of(TransactionDB(id = transactionId.toBigInteger()))).get()
        } catch (ex: NoSuchElementException) {
            logger.w("No transaction found by transactionId: '$transactionId'")
            throw ApiErrorException(ApiErrorType.TRANSACTION_NOT_FOUND)
        }

    }

    @Throws(ApiErrorException::class)
    private fun validateTransaction(
            executeTransactionParam: ExecuteTransactionParam,
            validateBalance: Boolean,
            sessionLog: JSessionLog
    ) {

        if (executeTransactionParam.fromUserLogin == executeTransactionParam.toUserLogin) {
            throw ApiErrorException(ApiErrorType.CIRCLE_TRANSACTION_NOT_SUPPORTED)
        }

        if (executeTransactionParam.amount <= 0) {
            throw ApiErrorException(ApiErrorType.AMOUNT_MUST_BE_POSITIVE)
        }

        val master = userManager.findUserByAuthToken(executeTransactionParam.authToken, sessionLog)
        val slave = userManager.findUserByLogin(executeTransactionParam.fromUserLogin, sessionLog)
        val hasPermissions = permissionsManager.allowUserManageUser(master, slave)
        if (!hasPermissions) {
            throw ApiErrorException(ApiErrorType.PERMISSION_DENIED)
        }

        if (validateBalance) {
            val fromBalance = balanceManager.getBalanceByLoginInner(executeTransactionParam.fromUserLogin, sessionLog)
            if (fromBalance < executeTransactionParam.amount) {
                val logger = sessionLog.getMethodLog(TransactionManager::class.java, "validateTransaction")
                logger.d("Balance ($fromBalance) < amount (${executeTransactionParam.amount})")
                throw ApiErrorException(ApiErrorType.ACCOUNT_OUTCOME_INSUFFICIENT_BALANCE)
            }
        }

    }

    private fun throwTransactionsIsDisabledException(): Nothing =
            throw ApiErrorException(ApiErrorType.UNDEFINED, "Пополнение счетов заблокировано")


}