package ru.hnau.suncamp.server.manager.transaction


object UserBalanceCache {

    private val cache = HashMap<String, Long>()

    fun clear() = cache.clear()

    fun get(login: String) = cache[login]

    fun set(login: String, balance: Long) {
        cache[login] = balance
    }

    fun inc(login: String, incValue: Long) = synchronized(cache) {
        val oldBalance = cache[login] ?: return@synchronized
        cache[login] = oldBalance + incValue
    }

}