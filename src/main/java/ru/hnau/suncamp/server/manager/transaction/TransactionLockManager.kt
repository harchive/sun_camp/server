package ru.hnau.suncamp.server.manager.transaction

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component
import ru.hnau.jutils.tryCatch
import ru.hnau.suncamp.server.data.db.TransactionLockDB
import ru.hnau.suncamp.server.data.repository.TransactionLockRepository


@Component
class TransactionLockManager {

    companion object {

        private const val KEY = "d0d20f80-9c83-424c-ac08-effb5a2d1ecc"

    }

    @Autowired
    lateinit var transactionLockRepository: TransactionLockRepository

    fun lock() {

        val transactionLock = TransactionLockDB(KEY)
        var blocked = true

        while (blocked) {
            tryCatch {
                transactionLockRepository.save(transactionLock)
                blocked = false
            }
        }
    }

    fun unlock() {
        transactionLockRepository.deleteById(KEY)
    }

}