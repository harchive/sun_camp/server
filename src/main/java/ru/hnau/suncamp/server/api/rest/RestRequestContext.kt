package ru.hnau.suncamp.server.api.rest

import ru.hnau.suncamp.server.utils.log.JSessionLog


data class RestRequestContext(
        val sessionLog: JSessionLog
)