package ru.hnau.suncamp.server.api.rest

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import org.springframework.web.context.request.RequestContextHolder
import org.springframework.web.context.request.ServletRequestAttributes
import ru.hnau.suncamp.common.api.ApiResult
import ru.hnau.suncamp.common.api.error.ApiErrorException
import ru.hnau.suncamp.common.api.error.ApiErrorType
import ru.hnau.suncamp.common.data.TransactionCategory
import ru.hnau.suncamp.server.manager.transaction.BalanceManager
import ru.hnau.suncamp.server.manager.CardManager
import ru.hnau.suncamp.server.manager.StorageManagerManager
import ru.hnau.suncamp.server.manager.transaction.TransactionManager
import ru.hnau.suncamp.server.manager.UserManager
import ru.hnau.suncamp.server.manager.transaction.ExecuteTransactionParam
import ru.hnau.suncamp.server.utils.log.JSessionLog


@RestController
class RestService {

    companion object {

        private const val MIN_SUPPORTED_VERSION = 4

    }

    @Autowired
    lateinit var userManager: UserManager

    @Autowired
    lateinit var cardManager: CardManager

    @Autowired
    lateinit var transactionManager: TransactionManager

    @Autowired
    lateinit var balanceManager: BalanceManager

    @Autowired
    lateinit var storageManagerManager: StorageManagerManager

    @RequestMapping("/api")
    fun index(version: Int) =
            execute(version, RequestByIpValidator.LIGHT_WEIGHT) {
                "SunCampService"
            }

    @RequestMapping("/api/admin/register-user")
    fun adminRegisterUser(login: String, password: String, name: String, version: Int) =
            execute(version, RequestByIpValidator.HARD_WEIGHT) {
                userManager.register(login, password, name, sessionLog)
            }

    @RequestMapping("/api/admin/invalidate-all-users-balances")
    fun adminInvalidateAllUserBalances(version: Int) =
            execute(version, RequestByIpValidator.HARD_WEIGHT) {
                balanceManager.invalidateAllUserBalances(sessionLog)
            }

    @RequestMapping("/api/admin/get-balance-statistic")
    fun adminGetBalanceStatistic(version: Int) =
            execute(version, RequestByIpValidator.HARD_WEIGHT) {
                balanceManager.getBalanceStatistic(sessionLog)
            }

    @RequestMapping("/api/user/login")
    fun userLogin(login: String, password: String, version: Int) =
            execute(version, RequestByIpValidator.HARD_WEIGHT) {
                userManager.login(login, password, sessionLog)
            }

    @RequestMapping("/api/user/update-password")
    fun userUpdatePassword(authToken: String, newPassword: String, version: Int) =
            execute(version, RequestByIpValidator.HARD_WEIGHT) {
                userManager.updatePassword(authToken, newPassword, sessionLog)
            }

    @RequestMapping("/api/user/update-name")
    fun userUpdateName(authToken: String, newName: String, version: Int) =
            execute(version, RequestByIpValidator.HARD_WEIGHT) {
                userManager.updateName(authToken, newName, sessionLog)
            }

    @RequestMapping("/api/user/get-all")
    fun userGetAll(version: Int) =
            execute(version, RequestByIpValidator.LIGHT_WEIGHT) {
                userManager.getAll()
            }

    @RequestMapping("/api/card/register")
    fun cardAdd(authToken: String, cardUuid: String, version: Int) =
            execute(version, RequestByIpValidator.HARD_WEIGHT) {
                cardManager.register(authToken, cardUuid, sessionLog)
            }

    @RequestMapping("/api/card/remove")
    fun cardRemove(authToken: String, cardUuid: String, version: Int) =
            execute(version, RequestByIpValidator.HARD_WEIGHT) {
                cardManager.remove(authToken, cardUuid, sessionLog)
            }

    @RequestMapping("/api/card/get-list")
    fun cardGetList(authToken: String, version: Int) =
            execute(version, RequestByIpValidator.LIGHT_WEIGHT) {
                cardManager.getList(authToken, sessionLog)
            }

    @RequestMapping("/api/card/get-user-by-card-uuid")
    fun cardGetUserByUuid(cardUuid: String, version: Int) =
            execute(version, RequestByIpValidator.LIGHT_WEIGHT) {
                cardManager.getUserByCardUuid(cardUuid, sessionLog)
            }

    @RequestMapping("/api/balance/get")
    fun balanceGet(authToken: String, login: String, version: Int) =
            execute(version, RequestByIpValidator.MEDIUM_WEIGHT) {
                balanceManager.getBalance(authToken, login, sessionLog)
            }

    @RequestMapping("/api/balance/get-of-users")
    fun balanceGetOfUsers(authToken: String, version: Int) =
            execute(version, RequestByIpValidator.MEDIUM_WEIGHT) {
                balanceManager.getBalancesOfUsers(authToken, sessionLog)
            }

    @RequestMapping("/api/transaction/get-list")
    fun transactionGetList(authToken: String, login: String, version: Int) =
            execute(version, RequestByIpValidator.MEDIUM_WEIGHT) {
                transactionManager.getList(authToken, login, sessionLog)
            }

    @RequestMapping("/api/storage-manager/add")
    fun storageManagerAdd(authToken: String, managerLogin: String, version: Int) =
            execute(version, RequestByIpValidator.HARD_WEIGHT) {
                storageManagerManager.addStorageManager(authToken, managerLogin, sessionLog)
            }

    @RequestMapping("/api/storage-manager/remove")
    fun storageManagerRemove(authToken: String, managerLogin: String, version: Int) =
            execute(version, RequestByIpValidator.HARD_WEIGHT) {
                storageManagerManager.removeStorageManager(authToken, managerLogin, sessionLog)
            }

    @RequestMapping("/api/storage-manager/get-subordinate-list")
    fun storageManagerGetSubordinateList(authToken: String, version: Int) =
            execute(version, RequestByIpValidator.LIGHT_WEIGHT) {
                storageManagerManager.getSubordinateList(authToken, sessionLog)
            }

    @RequestMapping("/api/transaction/execute")
    fun transactionExecute(
            authToken: String,
            fromUserLogin: String,
            toUserLogin: String,
            amount: Long,
            category: TransactionCategory,
            comment: String,
            version: Int
    ) =
            execute(version, RequestByIpValidator.HARD_WEIGHT) {
                transactionManager.execute(
                        ExecuteTransactionParam(
                                authToken,
                                fromUserLogin,
                                toUserLogin,
                                amount,
                                category,
                                comment,
                                null
                        ),
                        sessionLog
                )
            }

    @RequestMapping("/api/transaction/check-rollback")
    fun transactionCheckRollback(authToken: String, transactionId: Long, version: Int) =
            execute(version, RequestByIpValidator.LIGHT_WEIGHT) {
                transactionManager.checkRollback(authToken, transactionId, sessionLog)
            }

    @RequestMapping("/api/transaction/rollback")
    fun transactionRollback(authToken: String, transactionId: Long, version: Int) =
            execute(version, RequestByIpValidator.HARD_WEIGHT) {
                transactionManager.rollback(authToken, transactionId, sessionLog)
            }

    @RequestMapping("/api/transaction/from-storage-to-users")
    fun transactionFromStorageToUsers(
            authToken: String,
            fromStorageUserLogin: String,
            toUsersLogins: String,
            amount: Long,
            category: TransactionCategory,
            comment: String,
            version: Int
    ) =
            execute(version, RequestByIpValidator.HARD_WEIGHT) {
                transactionManager.fromStorageToUsers(
                        ExecuteTransactionParam(
                                authToken,
                                fromStorageUserLogin,
                                toUsersLogins,
                                amount,
                                category,
                                comment,
                                null
                        ),
                        sessionLog
                )
            }

    @RequestMapping("/api/transaction/from-users-to-storage")
    fun transactionFromUsersToStorage(
            authToken: String,
            fromUsersLogins: String,
            toStorageUserLogin: String,
            amount: Long,
            category: TransactionCategory,
            comment: String,
            version: Int
    ) =
            execute(version, RequestByIpValidator.HARD_WEIGHT) {
                transactionManager.fromUsersToStorage(
                        ExecuteTransactionParam(
                                authToken,
                                fromUsersLogins,
                                toStorageUserLogin,
                                amount,
                                category,
                                comment,
                                null
                        ),
                        sessionLog
                )
            }

    private fun execute(
            version: Int,
            requestWeight: Int,
            executor: RestRequestContext.() -> Any
    ) = executeWithResult(version, requestWeight) { ApiResult.success(executor.invoke(this)) }

    private fun executeWithResult(
            version: Int,
            requestWeight: Int,
            executor: RestRequestContext.() -> ApiResult<*>
    ): ApiResult<out Any> {

        val sessionLog = JSessionLog()
        val restRequestContext = RestRequestContext(sessionLog)
        val ip: String? = (RequestContextHolder.currentRequestAttributes() as ServletRequestAttributes).request.remoteAddr
        sessionLog.d("Request from: $ip")

        val response = try {
            RequestByIpValidator.validate(ip, requestWeight.toLong())
            checkVersion(version)
            executor.invoke(restRequestContext)
        } catch (ex: Exception) {
            if (ex !is ApiErrorException) {
                sessionLog.e("Unexpected exception", ex)
            }

            val apiErrorException = ex as? ApiErrorException
                    ?: ApiErrorException(ApiErrorType.UNDEFINED)

            ApiResult.error(apiErrorException.apiError)
        }

        sessionLog.d("Response: $response")
        return response
    }

    private fun checkVersion(version: Int) {
        if (version >= MIN_SUPPORTED_VERSION) {
            return
        }

        val versionUrl = "http://hnau.ru/suncamp-$MIN_SUPPORTED_VERSION.apk"

        throw ApiErrorException(
                ApiErrorType.UNSUPPORTED_VERSION,
                params = mapOf("updateUrl" to versionUrl)
        )

    }


}