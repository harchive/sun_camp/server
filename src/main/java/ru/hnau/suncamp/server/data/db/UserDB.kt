package ru.hnau.suncamp.server.data.db

import org.springframework.data.annotation.Id
import org.springframework.data.mongodb.core.mapping.Document
import ru.hnau.suncamp.common.data.User
import ru.hnau.suncamp.common.data.UserType

@Document(collection = "user")
data class UserDB(
        @Id val login: String? = null,
        val type: UserType? = null,
        val name: String? = null,
        val passwordHash: String? = null,
        val authToken: String? = null,
        val lastLogged: Long? = null,
        val baseAmount: Long? = null
) {

    constructor(user: User) : this(user.login, user.type, user.name)

    val user: User
        get() = User(login!!, type!!, name!!)

    override fun toString() =
            "UserDB{login=$login, type=$type, name=$name, passwordHash=<hidden>, authToken=$authToken, lastLogged=$lastLogged, baseAmount=$baseAmount}"


}


