package ru.hnau.suncamp.server.data.repository

import org.springframework.data.mongodb.repository.MongoRepository
import ru.hnau.suncamp.server.data.db.TransactionDB
import java.math.BigInteger


interface TransactionRepository : MongoRepository<TransactionDB, BigInteger>