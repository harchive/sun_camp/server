package ru.hnau.suncamp.server.data.db

import org.springframework.data.annotation.Id
import org.springframework.data.mongodb.core.mapping.Document
import ru.hnau.suncamp.common.data.Card
import java.math.BigInteger


@Document(collection = "card")
data class CardDB(
        @Id val uuid: String? = null,
        val associatedUserLogin: String? = null
) {

    constructor(card: Card) : this(card.uuid, card.associatedUserLogin)

    val card
        get() = Card(uuid!!, associatedUserLogin!!)

}