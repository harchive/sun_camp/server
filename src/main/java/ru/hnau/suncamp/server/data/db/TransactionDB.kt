package ru.hnau.suncamp.server.data.db

import org.springframework.data.annotation.Id
import org.springframework.data.mongodb.core.mapping.Document
import ru.hnau.suncamp.common.data.Transaction
import ru.hnau.suncamp.common.data.TransactionCategory
import java.math.BigInteger


@Document(collection = "transaction")
data class TransactionDB(
        @Id val id: BigInteger? = null,
        val fromUserLogin: String? = null,
        val toUserLogin: String? = null,
        val initiatorUserLogin: String? = null,
        val amount: Long? = null,
        val category: TransactionCategory? = null,
        val timestamp: Long? = null,
        val comment: String? = null,
        val associatedTransactionId: BigInteger? = null
) {

    constructor(transaction: Transaction) : this(
            transaction.id.toBigInteger(),
            transaction.fromUserLogin,
            transaction.toUserLogin,
            transaction.initiatorUserLogin,
            transaction.amount,
            transaction.category,
            transaction.timestamp,
            transaction.comment,
            transaction.associatedTransactionId?.toBigInteger()
    )

    val transaction: Transaction
        get() = Transaction(
                id!!.toLong(),
                fromUserLogin!!,
                toUserLogin!!,
                initiatorUserLogin!!,
                amount!!,
                category!!,
                timestamp!!,
                comment!!,
                associatedTransactionId?.toLong()
        )

}