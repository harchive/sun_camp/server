package ru.hnau.suncamp.server.utils.log

import ru.hnau.suncamp.server.utils.genBase64UUID


class JSessionLog(
        private val sessionId: String = genBase64UUID()
) : JLogWrapper(
        JBaseLog
) {

    override fun formatMessage(msg: String) = "{$sessionId} $msg"

    fun getMethodLog(className: String, methodName: String) = JMethodLog(this, className, methodName)

    fun getMethodLog(clazz: Class<*>, methodName: String) = JMethodLog(this, clazz, methodName)

}