package ru.hnau.suncamp.server.utils.log

import org.apache.logging.log4j.LogManager


object JBaseLog : JLog {

    private val logger = LogManager.getLogger()

    override fun i(msg: String, th: Throwable?) = logger.info(msg, th)

    override fun t(msg: String, th: Throwable?) = logger.trace(msg, th)

    override fun d(msg: String, th: Throwable?) = logger.debug(msg, th)

    override fun w(msg: String, th: Throwable?) = logger.warn(msg, th)

    override fun e(msg: String, th: Throwable?) = logger.error(msg, th)

    fun getSessionLog(sessionId: String) = JSessionLog(sessionId)

}