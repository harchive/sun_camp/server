package ru.hnau.suncamp.server.utils

import ru.hnau.jutils.tryCatch
import java.nio.ByteBuffer
import java.security.MessageDigest
import java.util.*
import java.util.UUID


private val MESSAGE_DIGEST_SHA_256 = MessageDigest.getInstance("SHA-256")

fun String.toHash() = synchronized(MESSAGE_DIGEST_SHA_256) {
    MESSAGE_DIGEST_SHA_256.update(this.toByteArray(Charsets.UTF_8))
    return@synchronized MESSAGE_DIGEST_SHA_256.digest().toString(Charsets.UTF_8)
}

fun genUUID() = UUID.randomUUID().toString()

fun genBase64UUID(): String {
    val uuid = UUID.randomUUID()
    val bb = ByteBuffer.wrap(ByteArray(16))
    bb.putLong(uuid.mostSignificantBits)
    bb.putLong(uuid.leastSignificantBits)
    return Base64.getEncoder().encodeToString(bb.array())
}

fun isUuid(str: String) = tryCatch { UUID.fromString(str) }

fun <T> List<T>.sumByLong(valueExtractor: (T) -> Long): Long {
    var result = 0L
    forEach { result += valueExtractor.invoke(it) }
    return result
}